# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
from tkinter import *
import random

# DEFINING SOME BASIC PRINCIPLES OF THE GAME
WIDTH_OF_GAME = 1200
HEIGHT_OF_GAME = 800
SPEED = 90
SPACE_SIZE = 100
BODY_PARTS = 3
snake_COLOUR = "#800080"  # the color purple
food_COLOUR = "#FFA500"  # the color orange
BACKGROUND_COLOUR = "#000000"  # the color black


# making the snake
class Snake:

    def __init__(self):
        self.body_size = BODY_PARTS
        self.squares_of_virtues = []
        self.x_y_coord = []

        for i in range(0, BODY_PARTS):
            # snake starts in the top left corner
            self.x_y_coord.append([0, 0])

        for x, y in self.x_y_coord:
            # snake form itself will be a rectangle
            # s_o_v == square_of_virtues
            s_o_v = game_canvas.create_rectangle(x, y, x + SPACE_SIZE, y + SPACE_SIZE, fill=snake_COLOUR,
                                                 tag="snakiee")
            self.squares_of_virtues.append(s_o_v)


# generating the food item
class Food:

    def __init__(self):
        x = random.randint(0, (WIDTH_OF_GAME / SPACE_SIZE) - 1) * SPACE_SIZE
        y = random.randint(0, (HEIGHT_OF_GAME / SPACE_SIZE) - 1) * SPACE_SIZE

        self.x_y_coord = [x, y]

        # defining all four sides of the food item
        game_canvas.create_rectangle(x, y, x + SPACE_SIZE, y + SPACE_SIZE, fill=food_COLOUR, tag="food")
        pass


# defining the next turn of the snake
def turn(snake, food):
    x, y = snake.x_y_coord[0]  # head of snake

    # think of this like apart of the four axis of graph
    # up = negation of gravity // Y axis
    # down = gravity // Y axis
    # left = backwards // X axis
    # right = forwards // X axis

    if direction == "down":
        y += SPACE_SIZE

    elif direction == "up":
        y -= SPACE_SIZE

    elif direction == "right":
        x += SPACE_SIZE

    elif direction == "left":
        x -= SPACE_SIZE

    snake.x_y_coord.insert(0, (x, y))  # aka the head x axis at 0 and where ever the head of the snake is

    s_o_v = game_canvas.create_rectangle(x, y, x + SPACE_SIZE, y + SPACE_SIZE, fill=snake_COLOUR)

    snake.squares_of_virtues.insert(0, s_o_v)

    if x == food.x_y_coord[0] and y == food.x_y_coord[1]:

        global score

        score += 1

        score_label.config(text="Score:{}".format(score))

        game_canvas.delete("food")

        food = Food()

    else:
        del snake.x_y_coord[-1]

        game_canvas.delete(snake.squares_of_virtues[-1])

        del snake.squares_of_virtues[-1]

    if check_for_collision(snake):
        game_over()

    else:
        window.after(SPEED, turn, snake, food)


# defining the direction in which the next turn will be
def direction_change(new_direction):
    global direction

    if new_direction == 'left':
        if direction != 'right':
            direction = new_direction

    elif new_direction == 'right':
        if direction != 'left':
            direction = new_direction

    elif new_direction == 'up':
        if direction != 'down':
            direction = new_direction

    elif new_direction == 'down':
        if direction != 'up':
            direction = new_direction


# defining when the snake would have have collided with itself
def check_for_collision(snake):
    x, y = snake.x_y_coord[0]

    if x < 0 or x >= WIDTH_OF_GAME:
        print("Game Over")
        return True

    elif y < 0 or y >= HEIGHT_OF_GAME:
        print("Game Over")
        return True

    for BODY_PARTS in snake.x_y_coord[1:]:
        if x == BODY_PARTS[0] and y == BODY_PARTS[1]:
            return True


# defining when the game is over
def game_over():
    game_canvas.delete(ALL)


# Title: Python snake game 🐍
# Author: Bro Code
# Date: Feb 11, 2021
# Code version: 2.0
# Availability: https://youtu.be/bfRwxS5d0SI

# making the window of the game where it'll be played
window = Tk()
window.title("snake Game")
# making it such that the window is not resizable
window.resizable(False, False)

# score label
score = 0
direction = 'down'

score_label = Label(window, text="Score:{}".format(score), font=('Courier New', 40))
score_label.pack()

game_canvas = Canvas(window, bg=BACKGROUND_COLOUR, height=HEIGHT_OF_GAME, width=WIDTH_OF_GAME)
game_canvas.pack()

window.update()

window_width = window.winfo_width()
window_height = window.winfo_height()
screen_width = window.winfo_screenwidth()
screen_height = window.winfo_screenheight()

# formula for balancing the window in the middle of the screen although i doubt this will be neccessary in the long run
w = int((screen_width / 2) - (window_width / 2))
z = int((screen_height / 2) - (window_height / 2))

# centering the window when it appears
window.geometry(f'{window_width}x{window_height}+{w}+{z}')

window.bind('<Right>', lambda event: direction_change('right'))
window.bind('<Left>', lambda event: direction_change('left'))
window.bind('<Up>', lambda event: direction_change('up'))
window.bind('<Down>', lambda event: direction_change('down'))

# generating the snake and food in the window
snake = Snake()
food = Food()

# calling the turn methods in the game window
turn(snake, food)

window.mainloop()
