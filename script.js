CanvasRenderingContext2D.prototype.roundRect = function (x, y, w, h, r) {
    if (w < 2 * r)
        r = w / 2;
    if (h < 2 * r)
        r = h / 2;
    this.beginPath();
    this.moveTo(x + r, y);
    this.arcTo(x + w, y, x + w, y + h, r);
    this.arcTo(x + w, y + h, x, y + h, r);
    this.arcTo(x, y + h, x, y, r);
    this.arcTo(x, y, x + w, y, r);
    this.closePath();
    return this;
};
var COLOR_TILE = '#222222';
var COLOR_TILE_BORDER = shade_color(COLOR_TILE, 0.3);
var NEON_COLORS = [
    '#33CC33',
    '#CCCC33',
    '#FF66AA',
    '#33CCCC',
    '#FF6600',
];
var MAX_GAME_SIZE = 20;
var FLASH_EDGES = false;
var ACTION_RATE = 200;
var SPEED_INCREMENT = 0.01;
var RENDER_RATE = 50;
var FLASH_RATE = ACTION_RATE * 4;
var CANVAS = document.getElementById('canvas');
var CONTEXT = CANVAS.getContext('2d');
var HAMMER = new Hammer(CANVAS);
var MOUSETRAP = new Mousetrap(CANVAS);
var MENU_IMAGE = document.getElementById('menu-image');
var Game = (function () {
    function Game() {
        this.loops = [];
        this.color_snake = NEON_COLORS[0];
        this.color_snake_border = shade_color(this.color_snake, -0.5);
        this.color_point = NEON_COLORS[1];
        this.color_point_border = shade_color(this.color_point, -0.5);
        this.flashing = false;
        this.wrap_around = true;
        this.accelerate_each_point = true;
        this.timer = new Timer();
        this.new_game();
        this.start_loops();
    }
    Game.prototype.new_game = function () {
        this.snake_nodes = [];
        this.points = [];
        this.facing = null;
        this.timer.reset();
        this.speed = 1;
        this.stats_display = new MessageBox();
        this.pause_button = new Button();
        this.set_game_size();
        this.spawn_player();
        this.spawn_point();
    };
    Game.prototype.set_game_size = function () {
        if (CANVAS.width > CANVAS.height) {
            this.width = MAX_GAME_SIZE;
            this.height = MAX_GAME_SIZE / (CANVAS.width / CANVAS.height);
        }
        else {
            this.height = MAX_GAME_SIZE;
            this.width = MAX_GAME_SIZE / (CANVAS.height / CANVAS.width);
        }
        this.height = this.height >> 0;
        this.width = this.width >> 0;
    };
    Game.prototype.start_loops = function () {
        clearTimeout(this.action);
        this.action = setTimeout(this.tick.bind(this), ACTION_RATE / this.speed);
        this.loops.push(setInterval(this.render.bind(this), RENDER_RATE));
        if (FLASH_EDGES) {
            this.loops.push(setInterval(this.flash_screen.bind(this), FLASH_RATE));
        }
        this.timer.start();
        this.render();
        HAMMER.off('tap');
        HAMMER.off('pan');
        this.bind_swipe_listeners();
        this.bind_tap_listeners();
        CANVAS.focus();
    };
    Game.prototype.spawn_player = function () {
        var xstart = this.width / 2 >> 0;
        var ystart = this.height / 2 >> 0;
        var head = [xstart, ystart];
        this.snake_nodes.push(head);
    };
    Game.prototype.spawn_point = function () {
        var potential = [];
        for (var _x = 0; _x < this.width; _x++) {
            for (var _y = 0; _y < this.height; _y++) {
                var snake_overlap = false;
                for (var _a = 0, _b = this.snake_nodes; _a < _b.length; _a++) {
                    var node = _b[_a];
                    if (_x === node[0] && _y === node[1]) {
                        snake_overlap = true;
                        break;
                    }
                }
                if (!snake_overlap) {
                    potential.push([_x, _y]);
                }
            }
        }
        var point = potential[rand_between(0, potential.length - 1)];
        this.points.push(point);
    };
    Game.prototype.render = function () {
        CONTEXT.clearRect(0, 0, CANVAS.width, CANVAS.height);
        for (var _x = 0; _x < this.width; _x++) {
            for (var _y = 0; _y < this.height; _y++) {
                this.draw_tile(_x, _y, COLOR_TILE, COLOR_TILE_BORDER);
            }
        }
        if (this.flashing) {
            CONTEXT.strokeStyle = COLOR_TILE_BORDER;
            CONTEXT.lineWidth = 10;
            CONTEXT.strokeRect(0, 0, CANVAS.width, CANVAS.height);
        }
        for (var _a = 0, _b = this.points; _a < _b.length; _a++) {
            var point = _b[_a];
            this.draw_tile(point[0], point[1], this.color_point, this.color_point_border);
        }
        for (var _c = 0, _d = this.snake_nodes; _c < _d.length; _c++) {
            var node = _d[_c];
            this.draw_tile(node[0], node[1], this.color_snake, this.color_snake_border);
        }
        if (!this.wrap_around) {
            CONTEXT.strokeStyle = '#CC0000';
            CONTEXT.lineWidth = 5;
            CONTEXT.strokeRect(0, 0, CANVAS.width, CANVAS.height);
        }
        var messages = [
            'Points: ' + this.snake_nodes.length,
            'Time: ' + this.timer.time(),
        ];
        this.stats_display.write_many(messages, '#CCC');
        this.pause_button.render();
    };
    Game.prototype.tick = function () {
        var last_point = this.snake_nodes[this.snake_nodes.length - 1];
        this.move_snake();
        var head = this.snake_nodes[0];
        for (var _i = this.points.length - 1; _i >= 0; _i--) {
            var point = this.points[_i];
            if (point[0] === head[0] && point[1] === head[1]) {
                this.snake_nodes.push(last_point);
                this.points.splice(_i, 1);
                this.spawn_point();
                this.change_colors();
                if (this.accelerate_each_point) {
                    this.speed += SPEED_INCREMENT;
                }
            }
        }
        this.action = setTimeout(this.tick.bind(this), ACTION_RATE / this.speed);
    };
    Game.prototype.move_snake = function () {
        if (this.snake_nodes.length > 1) {
            this.snake_nodes.pop();
            var newhead = [this.snake_nodes[0][0], this.snake_nodes[0][1]];
            this.snake_nodes.splice(0, 0, newhead);
        }
        var intent = [0, 0];
        if (this.facing === 'left') {
            intent[0] = -1;
        }
        else if (this.facing === 'right') {
            intent[0] = 1;
        }
        else if (this.facing === 'up') {
            intent[1] = -1;
        }
        else if (this.facing === 'down') {
            intent[1] = 1;
        }
        var head = this.snake_nodes[0];
        var destination = [head[0] + intent[0], head[1] + intent[1]];
        if (this.wrap_around) {
            if (destination[0] >= this.width) {
                destination[0] = 0;
            }
            else if (destination[0] < 0) {
                destination[0] = this.width - 1;
            }
            if (destination[1] >= this.height) {
                destination[1] = 0;
            }
            else if (destination[1] < 0) {
                destination[1] = this.height - 1;
            }
        }
        else {
            if (destination[0] >= this.width || destination[0] < 0 ||
                destination[1] >= this.height || destination[1] < 0) {
                this.lose_game();
                return;
            }
        }
        for (var _i = 1; _i < this.snake_nodes.length; _i++) {
            var node = this.snake_nodes[_i];
            if (destination[0] === node[0] && destination[1] === node[1]) {
                this.lose_game();
                return;
            }
        }
        this.snake_nodes[0] = destination;
        this.lastmove = this.facing;
    };
    Game.prototype.face = function (direction, unless) {
        if (this.lastmove !== unless) {
            this.facing = direction;
        }
    };
    Game.prototype.change_colors = function () {
        var points_eaten = this.snake_nodes.length - 1;
        if (points_eaten % 3 == 0) {
            var index = points_eaten / 3 % NEON_COLORS.length >> 0;
            this.color_snake = NEON_COLORS[index];
            this.color_snake_border = shade_color(this.color_snake, -0.5);
            var point_color_index = index + 1;
            if (point_color_index >= NEON_COLORS.length) {
                point_color_index = 0;
            }
            this.color_point = NEON_COLORS[point_color_index];
            this.color_point_border = shade_color(this.color_point, -0.5);
        }
    };
    Game.prototype.draw_tile = function (x, y, color, bordercolor) {
        var tile_width = CANVAS.width / this.width;
        var tile_height = CANVAS.height / this.height;
        var xstart = tile_width * x;
        var ystart = tile_height * y;
        CONTEXT.fillStyle = color;
        CONTEXT.fillRect(xstart, ystart, tile_width, tile_height);
        CONTEXT.strokeStyle = bordercolor;
        CONTEXT.lineWidth = 1;
        CONTEXT.strokeRect(xstart, ystart, tile_width, tile_height);
    };
    Game.prototype.toggle_pause = function () {
        if (this.loops.length === 0) {
            hide_game_menus();
            this.start_loops();
        }
        else {
            this.pause_game();
            show_game_menu('menu-main');
        }
    };
    Game.prototype.pause_game = function () {
        clearTimeout(this.action);
        for (var _a = 0, _b = this.loops; _a < _b.length; _a++) {
            var loop = _b[_a];
            clearInterval(loop);
        }
        this.loops = [];
        this.timer.pause();
        CONTEXT.globalAlpha = 0.5;
        CONTEXT.fillStyle = '#000';
        CONTEXT.fillRect(0, 0, CANVAS.width, CANVAS.height);
        CONTEXT.globalAlpha = 1;
    };
    Game.prototype.lose_game = function () {
        this.pause_game();
        show_game_menu('menu-game-over');
        var score = document.getElementById('game-over-score');
        score.innerHTML = this.snake_nodes.length;
        var time = document.getElementById('game-over-time');
        time.innerHTML = this.timer.time();
        var speed = document.getElementById('game-over-speed');
        speed.innerHTML = Math.round(this.speed * 100) / 100;
        this.new_game();
    };
    Game.prototype.bind_swipe_listeners = function () {
        HAMMER.on('pan', function (event) {
            if (event.direction == 2) {
                this.face('left', 'right');
            }
            else if (event.direction == 4) {
                this.face('right', 'left');
            }
            else if (event.direction == 8) {
                this.face('up', 'down');
            }
            else if (event.direction == 16) {
                this.face('down', 'up');
            }
        }.bind(this));
    };
    Game.prototype.bind_tap_listeners = function () {
        HAMMER.on('tap', function (event) {
            var center = event['center'];
            var zone_ratio = 0.3;
            var dead_ratio = 0.2;
            var leftzone = (center['x'] < CANVAS.width * zone_ratio &&
                center['y'] > CANVAS.height * (dead_ratio) &&
                center['y'] < CANVAS.height * (1 - dead_ratio));
            var rightzone = (center['x'] > CANVAS.width * (1 - zone_ratio) &&
                center['y'] > CANVAS.height * (dead_ratio) &&
                center['y'] < CANVAS.height * (1 - dead_ratio));
            var upzone = (center['y'] < CANVAS.height * zone_ratio &&
                center['x'] > CANVAS.width * (dead_ratio) &&
                center['x'] < CANVAS.width * (1 - dead_ratio));
            var downzone = (center['y'] > CANVAS.height * (1 - zone_ratio) &&
                center['x'] > CANVAS.width * (dead_ratio) &&
                center['x'] < CANVAS.width * (1 - dead_ratio));
            if (leftzone) {
                this.face('left', 'right');
            }
            else if (rightzone) {
                this.face('right', 'left');
            }
            else if (upzone) {
                this.face('up', 'down');
            }
            else if (downzone) {
                this.face('down', 'up');
            }
        }.bind(this));
        HAMMER.on('tap', function (event) {
            var _x = event['center']['x'];
            var _y = event['center']['y'];
            var xmin = CANVAS.width - this.pause_button.width;
            var xmax = CANVAS.width;
            var ymin = CANVAS.height - this.pause_button.height;
            var ymax = CANVAS.height;
            if (_x > xmin && _x < xmax && _y > ymin && _y < ymax) {
                this.toggle_pause();
            }
        }.bind(this));
    };
    Game.prototype.flash_screen = function () {
        if (!this.flashing) {
            this.flashing = true;
            setTimeout(function () {
                this.flashing = false;
            }.bind(this), FLASH_RATE / 8);
        }
    };
    return Game;
}());
var MessageBox = (function () {
    function MessageBox() {
        this.xanchor = 10;
        this.yanchor = CANVAS.height - 10;
        this.font = "20px Ubuntu";
        this.align = "left";
    }
    MessageBox.prototype.write_single = function (text, color) {
        CONTEXT.font = this.font;
        CONTEXT.textAlign = this.align;
        CONTEXT.fillStyle = color;
        CONTEXT.fillText(text, this.xanchor, this.yanchor);
    };
    MessageBox.prototype.write_many = function (messages, color) {
        CONTEXT.font = this.font;
        CONTEXT.textAlign = this.align;
        CONTEXT.fillStyle = color;
        for (var _i = 0; _i < messages.length; _i++) {
            var text = messages[_i];
            CONTEXT.fillText(text, this.xanchor, this.yanchor - _i * 20);
        }
    };
    return MessageBox;
}());
var Button = (function () {
    function Button() {
        this.width = 50;
        this.height = 50;
        this.xanchor = CANVAS.width - this.width;
        this.yanchor = CANVAS.height - this.height;
    }
    Button.prototype.render = function () {
        CONTEXT.drawImage(MENU_IMAGE, this.xanchor + 6.5, this.yanchor + 6.5, 32, 32);
        CONTEXT.lineWidth = 2;
        CONTEXT.strokeStyle = '#CCC';
        CONTEXT.roundRect(this.xanchor, this.yanchor, this.width - 5, this.height - 5, 10).stroke();
    };
    return Button;
}());
var Timer = (function () {
    function Timer(tps) {
        if (tps === void 0) { tps = 1; }
        this.current_seconds = 0;
        this.interval = null;
        this.tps = tps;
    }
    Timer.prototype.start = function () {
        this.interval = setInterval(this.tick.bind(this), 1000 / this.tps);
    };
    Timer.prototype.pause = function () {
        clearInterval(this.interval);
    };
    Timer.prototype.reset = function () {
        clearInterval(this.interval);
        this.current_seconds = 0;
    };
    Timer.prototype.tick = function () {
        this.current_seconds += 1;
    };
    Timer.prototype.time = function () {
        var hours = 0, minutes = 0, seconds = 0;
        var formatted = '';
        hours = (this.current_seconds / (60 * 60)) >> 0;
        if (hours > 0) {
            formatted += hours + ':';
        }
        minutes = (this.current_seconds / 60) >> 0;
        if (hours > 0 && minutes < 10) {
            formatted += '0';
        }
        formatted += minutes + ':';
        seconds = this.current_seconds % 60;
        if (seconds < 10) {
            formatted += '0';
        }
        formatted += seconds;
        return formatted;
    };
    return Timer;
}());
function bind_keyboard_listeners(game) {
    MOUSETRAP.bind(['a', 'A', 'left'], function () {
        game.face('left', 'right');
    }, 'keydown');
    MOUSETRAP.bind(['d', 'D', 'right'], function () {
        game.face('right', 'left');
    }, 'keydown');
    MOUSETRAP.bind(['w', 'W', 'up'], function () {
        game.face('up', 'down');
    }, 'keydown');
    MOUSETRAP.bind(['s', 'S', 'down'], function () {
        game.face('down', 'up');
    }, 'keydown');
    MOUSETRAP.bind(['esc', 'space', 'enter', 'p', 'P'], function () {
        game.toggle_pause();
    }, 'keydown');
}
function resize_canvas() {
    CANVAS.width = window.innerWidth;
    CANVAS.height = window.innerHeight;
    CONTEXT.strokeRect(0, 0, window.innerWidth, window.innerHeight);
}
function rand_between(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function shade_color(color, percent) {
    var f = parseInt(color.slice(1), 16), t = percent < 0 ? 0 : 255, p = percent < 0 ? percent * -1 : percent, R = f >> 16, G = f >> 8 & 0x00FF, B = f & 0x0000FF;
    var darker = ("#" +
        (0x1000000 +
            (Math.round((t - R) * p) + R) * 0x10000 +
            (Math.round((t - G) * p) + G) * 0x100 +
            (Math.round((t - B) * p) + B)).toString(16).slice(1));
    return darker;
}
function hide_game_menus() {
    var menus = document.getElementsByClassName('menu');
    for (var _i = 0; _i < menus.length; _i++) {
        menus[_i].style.display = 'none';
        menus[_i].style.zIndex = '-1';
    }
}
function show_game_menu(id) {
    hide_game_menus();
    document.getElementById(id).style.display = 'flex';
    document.getElementById(id).style.zIndex = '1';
}
window.onload = function () {
    resize_canvas();
    var game = new Game();
    document.ontouchmove = function (event) {
        event.preventDefault();
    };
    window.addEventListener('resize', function () {
        resize_canvas();
        game.pause_game();
        game.new_game();
        show_game_menu('menu-main');
    }.bind(game), false);
    bind_keyboard_listeners(game);
    hide_game_menus();
    document.getElementById('button-unpause').onclick = function () {
        game.toggle_pause();
    }.bind(game);
    document.getElementById('button-options').onclick = function () {
        show_game_menu('menu-options');
        var speed = document.getElementById('input-speed');
        speed.value = Math.round(game.speed * 100) / 100;
        var wrap_input = document.getElementById('input-wrap-around');
        wrap_input.value = game.wrap_around ? 'on' : 'off';
        var accel_input = document.getElementById('input-accelerate');
        accel_input.value = game.accelerate_each_point ? 'on' : 'off';
    };
    document.getElementById('button-rules').onclick = function () {
        show_game_menu('menu-rules');
    };
    document.getElementById('button-about').onclick = function () {
        show_game_menu('menu-about');
    };
    var back_buttons = document.getElementsByClassName('button-back-main');
    for (var index in back_buttons) {
        back_buttons[index].onclick = function () {
            show_game_menu('menu-main');
        };
    }
    var speed = document.getElementById('input-speed');
    speed.onchange = function () {
        game.speed = parseFloat(speed.value);
    };
    var wrap_input = document.getElementById('input-wrap-around');
    wrap_input.onchange = function () {
        game.wrap_around = wrap_input.value === 'on' ? false : true;
    };
    var accel_input = document.getElementById('input-accelerate');
    accel_input.onchange = function () {
        game.accelerate_each_point = accel_input.value === 'on' ? false : true;
    };
    var new_game_btn = document.getElementById('button-new-game');
    new_game_btn.onclick = function () {
        game.toggle_pause();
    };
    document.getElementById('loading-screen').style.display = 'none';
};
//# sourceMappingURL=script.js.map